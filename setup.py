
import sys
from setuptools import setup

UPLOAD = 'upload_sphinx' in sys.argv

if not UPLOAD and sys.version_info < (3, 7):
    raise RuntimeError("cstream requires at least Python 3.7")

with open("README.rst", encoding="utf-8") as f:
    long_description = f.read()

setup(
    name='cstream',

    packages=['cstream', 'cstream.stream'],
    setup_requires=["setuptools_scm"],
    tests_require=['pytest', 'pytest-cov'],
    use_scm_version=True,

    description="Generator-based operators for asynchronous iteration using Curio.",
    long_description=long_description,

    license="GPLv3",
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],

    author="Vincent Michel, Keith Dart",
    author_email="vxgmichel@gmail.com, keith.dart@gmail.com",
)

