"""Time-specific operators."""

import curio
from curio import traps

from ..aiter_utils import anext
from ..core import operator, streamcontext

__all__ = ['spaceout', 'delay', 'timeout']


async def wait_for(aw, timeout):
    async with curio.timeout_after(timeout):
        return await aw


@operator(pipable=True)
async def spaceout(source, interval):
    """Make sure the elements of an asynchronous sequence are separated
    in time by the given interval.
    """
    timeout = 0
    async with streamcontext(source) as streamer:
        async for item in streamer:
            ct = await traps._clock()
            delta = timeout - ct
            delay = delta if delta > 0 else 0
            await curio.sleep(delay)
            yield item
            ct = await traps._clock()
            timeout = ct + interval


@operator(pipable=True)
async def timeout(source, timeout):
    """Raise a time-out if an element of the asynchronous sequence
    takes too long to arrive.

    Note: the timeout is not global but specific to each step of
    the iteration.
    """
    async with streamcontext(source) as streamer:
        while True:
            try:
                item = await wait_for(anext(streamer), timeout)
            except StopAsyncIteration:
                break
            else:
                yield item


@operator(pipable=True)
async def delay(source, delay):
    """Delay the iteration of an asynchronous sequence."""
    await curio.sleep(delay)
    async with streamcontext(source) as streamer:
        async for item in streamer:
            yield item
