"""Selection operators."""

import collections

from curio import meta

from ..core import operator, streamcontext

__all__ = ['takelast', 'skip', 'skiplast', 'filter', 'until', 'dropwhile', 'takewhile']


@operator(pipable=True)
async def takelast(source, n):
    """Forward the last ``n`` elements from an asynchronous sequence.

    If ``n`` is negative, it simply terminates after iterating the source.

    Note: it is required to reach the end of the source before the first
    element is generated.
    """
    queue = collections.deque(maxlen=n if n > 0 else 0)
    async with streamcontext(source) as streamer:
        async for item in streamer:
            queue.append(item)
        for item in queue:
            yield item


@operator(pipable=True)
async def skip(source, n):
    """Forward an asynchronous sequence, skipping the first ``n`` elements.

    If ``n`` is negative, no elements are skipped.
    """
    i = 0
    async with streamcontext(source) as streamer:
        async for item in streamer:
            if i >= n:
                yield item
            i += 1


@operator(pipable=True)
async def skiplast(source, n):
    """Forward an asynchronous sequence, skipping the last ``n`` elements.

    If ``n`` is negative, no elements are skipped.

    Note: it is required to reach the ``n+1`` th element of the source
    before the first element is generated.
    """
    queue = collections.deque(maxlen=n if n > 0 else 0)
    async with streamcontext(source) as streamer:
        async for item in streamer:
            if n <= 0:
                yield item
                continue
            if len(queue) == n:
                yield queue[0]
            queue.append(item)


@operator(pipable=True)
async def filterindex(source, func):
    """Filter an asynchronous sequence using the index of the elements.

    The given function is synchronous, takes the index as an argument,
    and returns ``True`` if the corresponding should be forwarded,
    ``False`` otherwise.
    """
    i = 0
    async with streamcontext(source) as streamer:
        async for item in streamer:
            if func(i):
                yield item
            i += 1


@operator(pipable=True)
async def filter(source, func):
    """Filter an asynchronous sequence using an arbitrary function.

    The function takes the item as an argument and returns ``True``
    if it should be forwarded, ``False`` otherwise.
    The function can either be synchronous or asynchronous.
    """
    iscorofunc = meta.iscoroutinefunction(func)
    async with streamcontext(source) as streamer:
        async for item in streamer:
            result = func(item)
            if iscorofunc:
                result = await result
            if result:
                yield item


@operator(pipable=True)
async def until(source, func):
    """Forward an asynchronous sequence until a condition is met.

    Contrary to the ``takewhile`` operator, the last tested element is included
    in the sequence.

    The given function takes the item as an argument and returns a boolean
    corresponding to the condition to meet. The function can either be
    synchronous or asynchronous.
    """
    iscorofunc = meta.iscoroutinefunction(func)
    async with streamcontext(source) as streamer:
        async for item in streamer:
            result = func(item)
            if iscorofunc:
                result = await result
            yield item
            if result:
                return


@operator(pipable=True)
async def takewhile(source, func):
    """Forward an asynchronous sequence while a condition is met.

    Contrary to the ``until`` operator, the last tested element is not included
    in the sequence.

    The given function takes the item as an argument and returns a boolean
    corresponding to the condition to meet. The function can either be
    synchronous or asynchronous.
    """
    iscorofunc = meta.iscoroutinefunction(func)
    async with streamcontext(source) as streamer:
        async for item in streamer:
            result = func(item)
            if iscorofunc:
                result = await result
            if not result:
                return
            yield item


@operator(pipable=True)
async def dropwhile(source, func):
    """Discard the elements from an asynchronous sequence
    while a condition is met.

    The given function takes the item as an argument and returns a boolean
    corresponding to the condition to meet. The function can either be
    synchronous or asynchronous.
    """
    iscorofunc = meta.iscoroutinefunction(func)
    async with streamcontext(source) as streamer:
        async for item in streamer:
            result = func(item)
            if iscorofunc:
                result = await result
            if not result:
                yield item
                break
        async for item in streamer:
            yield item
