
import operator

import pytest
import curio

from cstream import stream, pipe

from .utils import add_resource


@pytest.mark.curio
async def test_aggregate(assert_run, testrunner):
    with testrunner.assert_cleanup():
        xs = stream.range(5) | add_resource.pipe(1) | pipe.accumulate()
        await assert_run(xs, [0, 1, 3, 6, 10])

    with testrunner.assert_cleanup():
        xs = (stream.range(2, 4) | add_resource.pipe(1) | pipe.accumulate(func=operator.mul, initializer=2))
        await assert_run(xs, [2, 4, 12])

    with testrunner.assert_cleanup():
        xs = stream.range(0) | add_resource.pipe(1) | pipe.accumulate()
        await assert_run(xs, [])

    async def sleepmax(x, y):
        await curio.sleep(1)
        return max(x, y)

    with testrunner.assert_cleanup():
        xs = stream.range(3) | add_resource.pipe(1) | pipe.accumulate(sleepmax)
        await assert_run(xs, [0, 1, 2])
        # assert testrunner.steps == [1]*3


@pytest.mark.curio
async def test_list(assert_run, testrunner):
    with testrunner.assert_cleanup():
        xs = stream.range(3) | add_resource.pipe(1) | pipe.list()
        await assert_run(xs, [[], [0], [0, 1], [0, 1, 2]])

    with testrunner.assert_cleanup():
        xs = stream.range(0) | add_resource.pipe(1) | pipe.list()
        await assert_run(xs, [[]])
