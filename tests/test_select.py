
import curio
import pytest

from cstream import stream, pipe

from .utils import add_resource


@pytest.mark.curio
async def test_takelast(assert_run, testrunner):
    xs = stream.range(10) | add_resource.pipe(1) | pipe.takelast(3)
    await assert_run(xs, [7, 8, 9])


@pytest.mark.curio
async def test_skip(assert_run, testrunner):
    xs = stream.range(10) | add_resource.pipe(1) | pipe.skip(8)
    await assert_run(xs, [8, 9])


@pytest.mark.curio
async def test_skiplast(assert_run, testrunner):
    with testrunner.assert_cleanup():
        xs = stream.range(10) | add_resource.pipe(1) | pipe.skiplast(8)
        await assert_run(xs, [0, 1])

    with testrunner.assert_cleanup():
        xs = stream.range(10) | add_resource.pipe(1) | pipe.skiplast(0)
        await assert_run(xs, list(range(10)))


@pytest.mark.curio
async def test_filterindex(assert_run, testrunner):
    filterindex = stream.select.filterindex
    xs = (stream.range(10) | add_resource.pipe(1) | filterindex.pipe(lambda x: x in [4, 7, 8]))
    await assert_run(xs, [4, 7, 8])


@pytest.mark.curio
async def test_filter(assert_run, testrunner):
    with testrunner.assert_cleanup():
        xs = (stream.range(1, 10) | add_resource.pipe(1) | pipe.filter(lambda x: x in [4, 7, 8]))
        await assert_run(xs, [4, 7, 8])

    async def afunc(x):
        await curio.sleep(1)
        return x in [3, 6, 9]

    with testrunner.assert_cleanup():
        xs = (stream.range(1, 10) | add_resource.pipe(1) | pipe.filter(afunc))
        await assert_run(xs, [3, 6, 9])
        # assert testrunner.steps == [1]*10


@pytest.mark.curio
async def test_until(assert_run, testrunner):
    with testrunner.assert_cleanup():
        xs = (stream.range(1, 10) | add_resource.pipe(1) | pipe.until(lambda x: x == 3))
        await assert_run(xs, [1, 2, 3])

    async def afunc(x):
        await curio.sleep(1)
        return x == 3

    with testrunner.assert_cleanup():
        xs = (stream.range(1, 10) | add_resource.pipe(1) | pipe.until(afunc))
        await assert_run(xs, [1, 2, 3])
        # assert testrunner.steps == [1] * 4


@pytest.mark.curio
async def test_takewhile(assert_run, testrunner):
    with testrunner.assert_cleanup():
        xs = (stream.range(1, 10) | add_resource.pipe(1) | pipe.takewhile(lambda x: x < 4))
        await assert_run(xs, [1, 2, 3])

    async def afunc(x):
        await curio.sleep(1)
        return x < 4

    with testrunner.assert_cleanup():
        xs = (stream.range(1, 10) | add_resource.pipe(1) | pipe.takewhile(afunc))
        await assert_run(xs, [1, 2, 3])
        # assert testrunner.steps == [1]*5


@pytest.mark.curio
async def test_dropwhile(assert_run, testrunner):
    with testrunner.assert_cleanup():
        xs = (stream.range(1, 10) | add_resource.pipe(1) | pipe.dropwhile(lambda x: x < 7))
        await assert_run(xs, [7, 8, 9])

    async def afunc(x):
        await curio.sleep(1)
        return x < 7

    with testrunner.assert_cleanup():
        xs = (stream.range(1, 10) | add_resource.pipe(1) | pipe.dropwhile(afunc))
        await assert_run(xs, [7, 8, 9])
        # assert testrunner.steps == [1]*8
