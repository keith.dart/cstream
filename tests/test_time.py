import pytest

from cstream import stream, pipe


@pytest.mark.curio
async def test_timeout(assert_run, testrunner):
    with testrunner.assert_cleanup():
        xs = stream.range(3) | pipe.timeout(5)
        await assert_run(xs, [0, 1, 2])
        assert testrunner.steps == []
