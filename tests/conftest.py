"""Pytest fixtures."""

from contextlib import contextmanager

from curio import sleep
from curio import traps

import pytest

from cstream.core import StreamEmpty, operator, streamcontext


def compare_exceptions(exc1, exc2):
    """Compare two exceptions together."""
    return (
        exc1 == exc2 or
        exc1.__class__ == exc2.__class__ and
        exc1.args == exc2.args)


async def assert_aiter(source, values, exception=None):
    """Check the results of a stream using a streamcontext."""
    it = iter(values)
    exception_type = type(exception) if exception else ()
    try:
        async with streamcontext(source) as streamer:
            async for item in streamer:
                assert item == next(it)
    except exception_type as exc:
        assert compare_exceptions(exc, exception)
    else:
        assert exception is None


async def assert_await(source, values, exception=None):
    """Check the results of a stream using by awaiting it."""
    exception_type = type(exception) if exception else ()
    try:
        result = await source
    except StreamEmpty:
        assert values == []
        assert exception is None
    except exception_type as exc:
        assert compare_exceptions(exc, exception)
    else:
        assert result == values[-1]
        assert exception is None


@pytest.fixture(params=[assert_aiter, assert_await], ids=['aiter', 'await'])
def assert_run(request):
    """Parametrized fixture returning a stream runner."""
    return request.param


@pytest.fixture
def testrunner():
    """Fixture providing a test runner for async tests.
    """

    class TimeTrackingTestHelper:

        STUCK_THRESHOLD = 100

        def __init__(self):
            self._time = 0
            self._timers = []
            self.clear()

        # Time management

        def time(self):
            return self._time

        def advance_time(self, advance):
            if advance:
                self._time += advance

        # TODO something that actually does something
#        def call_at(self, when, callback, *args, **kwargs):
#            self._timers.append(when)
#            # return super().call_at(when, callback, *args, **kwargs)

        @property
        def stuck(self):
            return self.busy_count > self.STUCK_THRESHOLD

        @property
        def time_to_go(self):
            return self._timers and (self.stuck or not self._ready)

        # Resource management
        # TODO make it work

        def clear(self):
            self.steps = []
            self.open_resources = 0
            self.resources = 0
            self.busy_count = 0

        @contextmanager
        def assert_cleanup(self):
            self.clear()
            yield self
            assert self.open_resources == 0
            self.clear()

    ttth = TimeTrackingTestHelper()
    with ttth.assert_cleanup():
        yield ttth
