# python3.7

# Copyright Ouster, Inc.

"""Utility functions for test cases.
"""

from curio import traps
from curio import sleep

from cstream.core import operator, streamcontext


@operator(pipable=True)
async def add_resource(source, cleanup_time):
    """Simulate an open resource in a stream operator."""

    try:
        kern = await traps._get_kernel()
        try:
            kern.open_resources += 1
        except AttributeError:
            kern.open_resources = 1
        try:
            kern.resources += 1
        except AttributeError:
            kern.resources = 1

        async with streamcontext(source) as streamer:
            async for item in streamer:
                yield item
    finally:
        try:
            await sleep(cleanup_time)
        finally:
            kern.open_resources -= 1
