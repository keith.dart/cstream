# python3.7

"""Tasks file for the *invoke* command.

To install:

    pip install invoke semver

Run `invoke <taskname>` to perform a test.  `invoke --list` will list all targets.
"""

from __future__ import annotations

import sys
import os
import shutil
import getpass
from glob import glob

import semver
from invoke import task, run, Exit


SIGNERS = ["keith.dart", "keith"]

PYTHONBIN = os.environ.get("PYTHONBIN", sys.executable)
# Put the path in quotes in case there is a space in it.
PYTHONBIN = f'"{PYTHONBIN}"'

GPG = "gpg2"

WINDOWS = sys.platform == "win32"
LINUX = sys.platform == "linux"
DARWIN = sys.platform == "darwin"

CURRENT_USER = getpass.getuser()


@task
def info(ctx):
    """Show information about the current Python and environment."""
    print(f"Python used: {PYTHONBIN}")
    venv = get_virtualenv()
    if venv:
        print(f"Virtual environment:", venv)


@task
def flake8(ctx):
    """Run flake8 linter on the package."""
    ctx.run(f"{PYTHONBIN} -m flake8 cstream")


@task
def build(ctx):
    """Build the intermediate package components."""
    ctx.run(f"{PYTHONBIN} setup.py build")


@task(build)
def install(ctx):
    """Build and do direct, local installation."""
    ctx.run(f"{PYTHONBIN} setup.py install --user --skip-build --optimize=1")


@task
def dev_requirements(ctx):
    ctx.run(f"sudo {PYTHONBIN} -m pip install -r dev-requirements.txt")


@task(pre=[dev_requirements])
def develop(ctx):
    """Start developing in developer mode."""
    ctx.run(f"{PYTHONBIN} setup.py develop --user")


@task
def clean(ctx):
    """Clean out build and cache files."""
    ctx.run(f"{PYTHONBIN} setup.py clean")
    if LINUX or DARWIN:
        ctx.run(r"find . -depth -type d -name __pycache__ -exec rm -rf {} \;")
        ctx.run('find cstream -name "*.so" -delete')


@task
def cleandist(ctx):
    if os.path.isdir("dist"):
        shutil.rmtree("dist", ignore_errors=True)
        os.mkdir("dist")


@task
def test(ctx, testfile=None, ls=False):
    """Run unit tests. Use ls option to only list them."""
    if ls:
        ctx.run("pytest --collect-only -qq tests")
    elif testfile:
        ctx.run(f"pytest -s {testfile}")
    else:
        ctx.run(f"{PYTHONBIN} setup.py test", hide=False, in_stream=False)


@task
def tag(ctx, tag=None, major=False, minor=False, patch=False):
    """Tag or bump release with a semver tag. Makes a signed tag."""
    latest = None
    if tag is None:
        tags = get_tags()
        if not tags:
            latest = semver.VersionInfo(0, 0, 0)
        else:
            latest = tags[-1]
        if patch:
            nextver = latest.bump_patch()
        elif minor:
            nextver = latest.bump_minor()
        elif major:
            nextver = latest.bump_major()
        else:
            nextver = latest.bump_patch()
    else:
        if tag.startswith("v"):
            tag = tag[1:]
        try:
            nextver = semver.parse_version_info(tag)
        except ValueError:
            raise Exit("Invalid semver tag.", 2)

    print(latest, "->", nextver)
    tagopt = "-s" if CURRENT_USER in SIGNERS else "-a"
    ctx.run(f'git tag {tagopt} -m "Release v{nextver}" v{nextver}')


@task
def tag_delete(ctx, tag=None):
    """Delete a tag, both local and remote."""
    if tag:
        ctx.run(f"git tag -d {tag}")
        ctx.run(f"git push origin :refs/tags/{tag}")


@task(cleandist)
def sdist(ctx):
    """Build source distribution."""
    ctx.run(f"{PYTHONBIN} setup.py sdist")


@task
def build_ext(ctx):
    ctx.run(f"{PYTHONBIN} setup.py build_ext --inplace")


@task(sdist)
def bdist(ctx):
    """Build a standard wheel file, an installable format."""
    ctx.run(f"{PYTHONBIN} setup.py bdist_wheel")


@task(bdist)
def sign(ctx):
    """Cryptographically sign dist with your default GPG key."""
    if CURRENT_USER in SIGNERS:
        skey = ctx.run("git config --get user.signingkey", hide="out").stdout.strip()
        ctx.run(f"{GPG} --detach-sign --default-key {skey} -a dist/cstream-*.whl")
        ctx.run(f"{GPG} --detach-sign --default-key {skey} -a dist/cstream-*.tar.gz")
    else:
        print("Not signing.")


@task
def push(ctx):
    """Push git workspace to repo."""
    ctx.run("git push")
    ctx.run("git push --tags")


@task(pre=[sign])
def publish(ctx):
    """Publish built wheel file to package repo."""
    distfiles = glob("dist/*.whl")
    distfiles.extend(glob("dist/*.tar.gz"))
    if not distfiles:
        raise Exit("Nothing in dist folder!")
    distfiles = " ".join(distfiles)
    ctx.run(f'{PYTHONBIN} -m twine upload {distfiles}')


@task
def register(ctx):
    """Register the package with the packer repo. Only needed first time."""
    distfiles = glob("dist/*.whl")
    if not distfiles:
        raise Exit("Nothing in dist folder!")
    distfile = distfiles[0]
    ctx.run(f'{PYTHONBIN} -m twine register {distfile}')


@task
def docs(ctx):
    """Build the HTML documentation."""
    with ctx.cd("docs"):
        ctx.run(f"{PYTHONBIN} -m sphinx.cmd.build -M html . _build")
    if LINUX:
        ctx.run("xdg-open docs/_build/html/index.html")


@task
def branch(ctx, name=None):
    """start a new branch, both local and remote tracking."""
    if name:
        ctx.run(f"git checkout -b {name}")
        ctx.run(f"git push -u origin {name}")
    else:
        ctx.run(f"git --no-pager branch")


@task
def branch_delete(ctx, name=None):
    """Delete local, remote and tracking branch by name."""
    if name:
        ctx.run(f"git branch -d {name}", warn=True)  # delete local branch
        ctx.run(f"git branch -d -r {name}", warn=True)  # delete local tracking info
        ctx.run(f"git push origin --delete {name}", warn=True)  # delete remote (origin) branch.
    else:
        print("Supply a branch name: --name <name>")


# Helper functions follow.
def get_tags():
    rv = run('git tag -l "v*"', hide="out")
    vilist = []
    for line in rv.stdout.split():
        try:
            vi = semver.parse_version_info(line[1:])
        except ValueError:
            pass
        else:
            vilist.append(vi)
    vilist.sort()
    return vilist


def get_virtualenv():
    venv = os.environ.get("VIRTUAL_ENV")
    if venv and os.path.isdir(venv):
        return venv
    return None
