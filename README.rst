cstream
=========

Generator-based operators for asynchronous iteration using Curio.
Forked from the aiostream_ package and renamed to cstream (for Curio Stream).

Many thanks to Vincent Michel for the aiostream_ package on which this is based. That code needed
extensive changes to adapt to the Curio design. Therefore, it needed to be forked and renamed.


Synopsis
--------

cstream_ provides a collection of stream operators that can be combined to create
asynchronous pipelines of operations.

It can be seen as an asynchronous version of itertools_, although some aspects are slightly different.
Essentially, all the provided operators return a unified interface called a stream.
A stream is an enhanced asynchronous iterable providing the following features:

- **Operator pipe-lining** - using pipe symbol ``|``
- **Repeatability** - every iteration creates a different iterator
- **Safe iteration context** - using ``async with`` and the ``stream`` method
- **Simplified execution** - get the last element from a stream using ``await``
- **Slicing and indexing** - using square brackets ``[]``
- **Concatenation** - using addition symbol ``+``


Requirements
------------

The stream operators rely heavily on asynchronous generators (`PEP 525`_):

- python >= 3.6


Stream operators
----------------

The `stream operators`_ are separated in 7 categories:

+--------------------+---------------------------------------------------------------------------------------+
| **creation**       | iterate_, preserve_, just_, call_, empty_, throw_, never_, repeat_, count_, range_    |
+--------------------+---------------------------------------------------------------------------------------+
| **selection**      | take_, takelast_, skip_, skiplast_, getitem_, filter_, until_, takewhile_, dropwhile_ |
+--------------------+---------------------------------------------------------------------------------------+
| **combination**    | map_, zip_, merge_, chain_, ziplatest_                                                |
+--------------------+---------------------------------------------------------------------------------------+
| **aggregation**    | accumulate_, reduce_, list_                                                           |
+--------------------+---------------------------------------------------------------------------------------+
| **timing**         | spaceout_, timeout_, delay_                                                           |
+--------------------+---------------------------------------------------------------------------------------+
| **miscellaneous**  | action_, print_                                                                       |
+--------------------+---------------------------------------------------------------------------------------+


Demonstration
-------------

The following example demonstrates most of the streams capabilities:

.. sourcecode:: python

    from cstream import stream, pipe


    async def main():

        # Create a counting stream with a 0.2 seconds interval
        xs = stream.count(interval=0.2)

        # Operators can be piped using '|'
        ys = xs | pipe.map(lambda x: x**2)

        # Streams can be sliced
        zs = ys[1:10:2]

        # Use a stream context for proper resource management
        async with zs.stream() as streamer:

            # Asynchronous iteration
            async for z in streamer:

                # Print 1, 9, 25, 49 and 81
                print('->', z)

        # Streams can be awaited and return the last value
        print('9² = ', await zs)

        # Streams can run several times
        print('9² = ', await zs)

        # Streams can be concatenated
        one_two_three = stream.just(1) + stream.range(2, 4)

        # Print [1, 2, 3]
        print(await stream.list(one_two_three))


    # Run main coroutine
    kern = curio.Kernel()
    kern.run(main())

More examples are available in the `example section`_ of the documentation.


Contact
-------

Vincent Michel: vxgmichel@gmail.com
Keith Dart: keith.dart@gmail.com


.. _aiostream: https://github.com/vxgmichel/aiostream
.. _cstream: https://github.com/kdart/cstream
.. _PEP 525: http://www.python.org/dev/peps/pep-0525/
.. _itertools: http://docs.python.org/3/library/itertools.html
