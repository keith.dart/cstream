Core objects
============

.. module:: cstream.core

Stream base class
-----------------

.. autoclass:: Stream
   :members:


Stream context manager
----------------------

.. autofunction:: streamcontext


Operator decorator
------------------

.. autofunction:: operator
