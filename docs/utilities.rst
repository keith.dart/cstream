Utilities
=========

cstream_ also provides utilites for general asynchronous iteration and asynchronous context management.

Asynchronous iteration
----------------------

.. automodule:: cstream.aiter_utils
   :members:
   :exclude-members: AsyncExitStack

.. _cstream: https://github.com/kdart/cstream
