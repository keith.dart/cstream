+++++++++
cstream
+++++++++

Generator-based operators for asynchronous iteration for users of the Curio framework.

.. toctree::
   :maxdepth: 1

   presentation
   operators
   core
   examples
   utilities

Reference table:

.. module:: cstream.stream
.. include:: table.rst.inc
